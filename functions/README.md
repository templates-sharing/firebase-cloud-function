# Setup
1. Install firebase cli
```
npm install -g firebase-tools
```
2. Login firebase
```
firebase login
```

# Start local host
```
npm run serve
```

# Deploy to firebase cloud
```
npm run deploy 
```