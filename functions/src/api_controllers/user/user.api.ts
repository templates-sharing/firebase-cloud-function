import {db} from "../../firebase_controller/firebase.admin";

export const getUserInfo = async (uid: string) => {
  if (!uid) return {status: 404, data: "Can not dettect user"};
  try {
    const data = await db.doc(`users/${uid}`).get();
    if (data.exists) {
      return {status: 200, data: data.data()};
    }
    return {status: 404, data: null};
  } catch (er) {
    return {status: 500, data: er};
  }
};
