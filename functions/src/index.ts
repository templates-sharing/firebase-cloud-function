import express = require("express");
import {onCall, onRequest} from "firebase-functions/v2/https";
import {getUserInfo} from "./api_controllers/user/user.api";
const app = express();
// eslint-disable-next-line @typescript-eslint/no-var-requires
const cors = require("cors")({origin: true});
app.use("*", cors);

app.get("*", async (request, response) => {
  const requestedUid = request.body.uid;
  let responseData = {status: 200, data: null as any};
  switch (request.originalUrl) {
  case "/get_user_info":
    responseData = await getUserInfo(requestedUid);
    break;
  }
  response.status(responseData.status).send(responseData.data);
});
app.post("*", (request, response) => {
  console.log(request.originalUrl);
  response.send("api does not support!");
});

exports.api = onRequest(app);

exports.get_user_info = onCall(async (request) => {
  const requestedUid = request.auth?.uid;
  console.log("call get_user_info", requestedUid);
  if (!requestedUid) {
    return {message: "Can not dettect user!"};
  }

  const responseData = await getUserInfo(requestedUid);
  return responseData.data;
});
